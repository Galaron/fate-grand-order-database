import axios from 'axios';
import * as fs from 'fs';
import * as path from 'path';
import { JSDOM } from 'jsdom';
import { info } from 'console';

type Servant = {
	name: string;
	rarity: number;
	ID: number;
	gender: string;
	wikiUrl: string;
	alignment: string[];
	servantClass: string;
	npType: string[];
	traits: string[];
	npCategory: string;
	costumeDress: number;
	limited: string[];
};

type ServantLight = Pick<Servant, 'ID' | 'name' | 'rarity' | 'wikiUrl'>;

enum ReportStatus {
	'INITIAL',
	'STARTED',
	'FAILED',
	'COMPLETED',
}

type ServantReport = {
	status: ReportStatus;
	lightData: ServantLight;
	data: null | Servant;
};

const BASE_URL = 'https://fategrandorder.fandom.com';

type FetchServantType = (url: string) => Promise<Pick<Servant, 'gender' | 'alignment' | 'servantClass' | 'npType' | 'traits'>>;

const fetchServant: FetchServantType = async (url) => {
	const x = await axios.get(url);

	const html = new JSDOM(x.data);

	const infoTable = html.window.document.querySelector('div.ServantInfoStatsWrapper table tbody');
	const tableRowsArray: HTMLTableRowElement[] = [];
	infoTable?.querySelectorAll('tr')?.forEach((el) => tableRowsArray.push(el));

	const gender = tableRowsArray[11]?.querySelector('td')?.innerHTML.split(' ')[1].split('\n')[0] ?? '';

	const alignment = tableRowsArray[10]?.querySelector('td:nth-child(2)')?.innerHTML.split('</b> ')[1].split('\n')[0].split('・') ?? [];

	const infoClass = html.window.document.querySelector('div.ServantInfoClass a');
	const servantClass = infoClass?.getAttribute('title') ?? '';

	const npInfo = html.window.document.querySelector('a[title="Noble Phantasm List"] img');
	const npType = [npInfo?.getAttribute('alt')?.split('.')[0] ?? ''];

	const traits = tableRowsArray[12]?.querySelector('td')?.innerHTML.split('</b> ')[1].split('\n')[0].split(', ') ?? [];

	return { gender, alignment, servantClass, npType, traits };
};

const fetchServants = async () => {
	const x = await axios.get(`${BASE_URL}/wiki/Servant_List_by_ID`);

	const servantsLightList: ServantLight[] = [];
	const html = new JSDOM(x.data);
	html.window.document.querySelectorAll('table.wikitable.sortable').forEach((table) => {
		const servantRows = table?.querySelectorAll('tr');
		const servantRowsArray: HTMLTableRowElement[] = [];
		servantRows?.forEach((el) => servantRowsArray.push(el));

		servantRowsArray?.shift();

		servantRowsArray?.forEach(async (row) => {
			const rowCells: HTMLTableCellElement[] = [];
			row.querySelectorAll('td')?.forEach((el) => rowCells.push(el));

			const wikiUrl = `${BASE_URL}${rowCells[1].querySelector('a')?.href}`;
			const name = rowCells[1].querySelector('a')?.innerHTML ?? '';
			const rarity = rowCells[2].innerHTML.length / 2;
			const ID = parseInt(rowCells[3].innerHTML);

			servantsLightList.push({
				name,
				wikiUrl,
				rarity,
				ID,
			});
		});
	});

	console.log(`Fetched ${servantsLightList.length} servants light data`);

	let servantsReport: ServantReport[] = [];

	servantsLightList.forEach((servant) => {
		servantsReport.push({
			status: ReportStatus.INITIAL,
			lightData: servant,
			data: null,
		});
	});

	let cycle = 1;
	while (servantsReport.filter(({ status }) => status !== ReportStatus.COMPLETED).length > 0) {
		const filteredReports = servantsReport.filter(({ status }) => status !== ReportStatus.COMPLETED);
		console.log(`(cycle ${cycle}) Fetching details for ${filteredReports.length} servants`);
		servantsReport = await Promise.all(
			servantsReport.map(async (rep, index) => {
				if (rep.status === ReportStatus.COMPLETED) {
					return rep;
				}
				const returnRep = { ...rep };
				const { ID, name, rarity, wikiUrl } = rep.lightData;
				console.log(`(${cycle}: ${index}/${filteredReports.length}) Fetching details for ${name}`);

				try {
					returnRep.status = ReportStatus.STARTED;
					const { gender, alignment, servantClass, npType, traits } = await fetchServant(wikiUrl);
					const npCategory = 'ST or AoE or Non-Damaging';
					const costumeDress = 0;
					const limited = ['FP only', 'Story Locked', 'Event', 'Limited'];

					const servant: Servant = {
						ID,
						name,
						rarity,
						wikiUrl,
						servantClass,
						gender,
						alignment,
						traits,
						npType,
						npCategory,
						costumeDress,
						limited,
					};
					console.log(`(${cycle}: ${index}/${filteredReports.length}) Fetching details for ${name} COMPLETED`);
					returnRep.data = servant;
					returnRep.status = ReportStatus.COMPLETED;
				} catch (error) {
					console.log(`(${cycle}: ${index}/${filteredReports.length}) Fetching details for ${name} FAILED`);
					console.log(error);
					returnRep.status = ReportStatus.FAILED;
				} finally {
					return returnRep;
				}
			}),
		);
		cycle += 1;
	}

	console.log(`Fetched details for ${servantsReport.length}`);

	const servants = servantsReport.map(({ data }) => data);

	servants.sort((a, b) => (a?.ID ?? 0) - (b?.ID ?? 0));

	await fs.writeFileSync('./servants.json', JSON.stringify(servants, null, 2));

	return servants;
};

const servants = fetchServants();
